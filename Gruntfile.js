/*global module:false*/
module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({

    bowerInstall: {

      target: {
        src: [
          'app/index.html' // .html support...
        ],

        // Optional:
        // ---------
        cwd: '',
        dependencies: true,
        devDependencies: false,
        exclude: [],
        fileTypes: {},
        ignorePath: '',
        overrides: {}
      }
    },
    watch: {
      bower: {
        files: ['bower.json'],
      },
      live: {
        files: ['app/*.*'],
        tasks: [],
        options: {
          livereload: true
        }
      }
    },
    connect: {
      server: {
        options: {
          base: '.',
          livereload: true
        }
      }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-connect');
  grunt.loadNpmTasks('grunt-bower-install');

  // Default task.
  grunt.registerTask('bower', ['bowerInstall', 'watch:bower']);
  grunt.registerTask('default', [ 'bowerInstall', 'connect', 'watch']);

};
