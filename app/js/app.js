/**
 * Created by rgoyard on 07/11/2015.
 */
(function () {
   'use strict';

    var classMenuActive= 'list-group-item-info';
    var defaultPage = "home";

    /**
     * Load the menu from the server then call the callback function with the menu object
     *
     * @param {Function} callback
     */
    function loadMenu(callback) {
        $.get('/menu.adoc', function (adoc) {
            var options = Opal.hash({safe: 'safe',attributes: ['showtitle']});
            var htmlContent = Opal.Asciidoctor.$convert(adoc, options);
            callback.call({}, htmlContent)
        }).fail(function () {

        });
    }



    /**
     * Retrieve the page from the server and then render it.
     *
     * @param page (The page with .adoc extension)
     */
    function show(page) {
        $.get('/' + page, function (adoc) {
            var options = Opal.hash({safe: 'safe',attributes: ['showtitle']});
            var htmlContent = Opal.Asciidoctor.$convert(adoc, options);

            $('#main').html(htmlContent);
            $("#main").find('pre.highlight').addClass('prettyprint');
            prettyPrint();
        }).fail(function () {
            $('#main').html("<h2>Page Not Found : " + page + "</h2>");
        });
    }


    /**
     * Load the page from Ajax then load menu selection
     */
    function load(page){
        show(page+'.adoc');
        selectMenuItem(page);
    }

    function printItem(item, parent) {
        return '<li class="list-group-item" ><a href="#' + parent + item.url + '">' + item.label + '</a></li>';
    }

    function printDirectoryMenu(element, parent) {
        var html = '';

        element.items.forEach(function (item) {
            if (item.hasOwnProperty('items') && Array.isArray(item.items)) {
                html += '<li class="list-group-item disabled">'+item.label+'</li>'
                html += printDirectoryMenu(item, parent + item.url + '/') + '</li>';
            } else {
                html += printItem(item, parent);
            }
        });


        return html;
    }

    /**
     * Print the menu
     */
    function menu() {

        loadMenu(function (html) {
            $('#menu').html(html);
            $('#menu').find('a').on('click', function () {
                $('#menu').find('p.'+classMenuActive).removeClass(classMenuActive);
                $(this).parent().addClass(classMenuActive);
                var page = $(this).attr('href').substr(2) + '.adoc';
                show(page);
            });
        });
    }


    menu();
    var hash = window.location.hash;

    function selectMenuItem(hash) {
        $('#menu').find('p.'+classMenuActive).removeClass(classMenuActive);
        $('a[href="' + hash + '"]').parent().addClass(classMenuActive);
    }

    function goTo(page){
        window.location.href='#/'+page;
    }

    if (hash === null | hash === '') {
        goTo('home');
    } else {
        load(hash.substr(2));
    }


    window.addEventListener("hashchange", function(){
      var hash = window.location.hash;
      load(hash.substr(2));
    }, false);

})();
