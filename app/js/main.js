(function (plugins) {
    'use strict';

    var config_file = 'config.json',
        classMenuActive = 'selected',
        config = null,
        currentHash = null,
        previousHash = null,
        currentClassName = null;


    /**
     * Parse the adoc content to HTML
     * @param adoc
     * @returns {HTMLString}
     */
    function parse(adoc) {
        var options = Opal.hash({safe: 'safe', attributes: ['showtitle']});
        return Opal.Asciidoctor.$convert(adoc, options);
    }


    /**
     * Retrieve the page from the server and then render it.
     *
     * @param page (The page with .adoc extension)
     */
    function show(page) {
        $.get(page).then(function (adoc) {
            display(parse(adoc));
            postProcessing();
            applyPlugins();
        }, function () {
            display("ERROR : Page Not Found " + page, 'error');
        });
    }


    /**
     * Return the value of the attribute attributeName for the element
     *
     * @param element {Element}
     * @param attributeName {String}
     * @returns {*}
     */
    function getAttributeValue(element, attributeName) {
        if (element.hasAttributes()) {
            var flag = null;
            Array.prototype.slice.call(element.attributes).forEach(function (item) {
                if (item.name === attributeName) {
                    flag = item.value;
                }
            });
            return flag;
        } else {
            return null;
        }
    }

    /**
     * Post processing stuff after displaying the page
     */
    function postProcessing() {
        var links = getMain().querySelectorAll('a');
        // We need to transform the nodeList return by querySelectorAll
        links = Array.prototype.slice.call(links);
        links.forEach(function (element) {
            element.addEventListener('click', function (e) {
                e.preventDefault();
                var href = getAttributeValue(this, 'href');

                if (href !== null) {
                    var id = href.substr(1);

                    if (document.getElementById(id)) {
                        scrollTo(document.getElementById(id));
                    } else {
                        window.location.href = href;
                    }
                }

            }, false);
        })
    }

    /**
     * Find the position of the element
     *
     * @param obj {Element}
     * @returns {*[]}
     */
    function findPos(obj) {
        var curtop = 0;
        if (obj.offsetParent) {
            do {
                curtop += obj.offsetTop;
            } while (obj = obj.offsetParent);
            return [curtop];
        }
    }

    /**
     * Scroll to an element in the page
     *
     * @param element {Element} The element to scroll To
     */
    function scrollTo(element) {
        window.scroll(0, findPos(element));
    }

    /**
     * get the main element.
     *
     * @returns {Element}
     */
    function getMain() {
        return document.querySelector("#wrapper section article");
    }

    /**
     * get the menu element.
     *
     * @returns {Element}
     */
    function getMenu() {
        return document.querySelector("#wrapper section nav");
    }

    /**
     * Display the content in the main part of the app
     *
     * @param content
     * @param mode
     */
    function display(content, mode) {
        getMain().innerHTML = content;
        if (mode === 'error') {
            getMain().classList.add('error');
        } else {
            getMain().classList.remove('error');
        }
        addClassToMain();
    }

    /**
     * Add a css class to the main regarding the content
     */
    function addClassToMain() {
        var hash = window.location.hash;
        var className = hash.substr(2);
        if (currentClassName) {
            getMain().classList.remove(currentClassName);
        }
        console.log(document.referer);
        getMain().classList.add(className);
        currentClassName = className;
    }

    /**
     * Apply plugins on the main content
     */
    function applyPlugins() {
        plugins.forEach(function (plugin) {
            plugin.call({}, getMain());
        });
    }


    /**
     * Load the page from Ajax then load menu selection
     */
    function load(page) {
        show(page);
        selectMenuItem();
    }


    /**
     * Find parent element by selector
     *
     * @param element {Element} the starting point
     * @param selector {String} the selector we are looking for
     * @returns {*}
     */
    function findParentElement(element, selector) {
        if (element.parentElement !== null && element.parentElement !== undefined) {
            var parent = element.parentElement;
            while (parent && !parent.matches(selector)) {
                parent = findParentElement(parent, selector);
            }
            return parent;
        } else {
            return null;
        }
    }


    /**
     * Add the select class to the curent menu element regarding the current page
     */
    function selectMenuItem() {
        var applySelectClassOn = 'li';
        var activeItem = getMenu().querySelector(applySelectClassOn + '.' + classMenuActive);
        if (activeItem) {
            activeItem.classList.remove(classMenuActive);
        }
        var selector = 'a[href="' + window.location.hash + '"]';
        var parent = findParentElement(document.querySelector(selector), applySelectClassOn);
        if (parent) {
            parent.classList.add(classMenuActive);
        }
    }

    /**
     * Display the Menu
     */
    function menu(page) {
        $.get(page).then(function (adoc) {
            getMenu().innerHTML = parse(adoc);
            selectMenuItem();
        }, function () {
            getMenu().innerHTML = "ERROR : Unable to find menu : " + page;
        });
    }

    /**
     * Display the header Title
     * @param title
     */
    function header(string) {
        var title = document.querySelector("#wrapper header");
        title.innerHTML = '<h1>' + string + '</h1>';
        document.querySelector('title').innerHTML = string;
    }


    /**
     * Initialize main events
     */
    function initEvents() {
        console.debug('METHOD : initEvents');
        window.addEventListener("hashchange", function (e) {
            console.debug('EVENT : hashchange');
            console.dir(e);
            var hash = window.location.hash;
            currentHash = hash;
            previousHash = e.oldURL.substr(e.oldURL.lastIndexOf('#'));
            if (config) {
                load(config.docurl + "/" + hash.substr(2) + '.adoc');
            }
        }, false);
    }

    /**
     * Initialize the application
     */
    function init() {
        currentHash = window.location.hash;
        $.get(config_file).then(function (response) {
            try {
                config = JSON.parse(response);
                header(config.title);
                menu(config.docurl + "/" + config.menu)

                // check current page
                if (currentHash === null || currentHash === '') {
                    load(config.docurl + "/" + config.home);
                } else {
                    load(config.docurl + "/" + currentHash.substr(2) + '.adoc');
                }


            } catch (err) {
                display("ERROR : Unable to parse config file " + err, 'error');
            }

        }).catch(function () {
            display("ERROR : Unable find config file.", 'error');
        });
    }

    // Run the application
    init();
    initEvents();
})([]);