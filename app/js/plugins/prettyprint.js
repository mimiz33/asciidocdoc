/**
 * This plugin allow to add syntax highlighting in ascii doc documents.
 * This plugin should be applied to rendered html
 *
 * @author Rémi Goyard
 * @version 1.O
 * @depends "prettify-small": "https://github.com/google/code-prettify/raw/master/distrib/prettify-small.zip"
 *          "sunburst": "https://raw.githubusercontent.com/google/code-prettify/master/styles/sunburst.css"
 */
(function(){
    'use strict';
    return function (main) {
        var elements = main.querySelectorAll('pre.highlight');
        for (var i = 0; i < elements.length; i++) {
            elements[i].classList.add('prettyprint');
        }
        prettyPrint();
    }
})();