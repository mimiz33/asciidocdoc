var $ = (function(){
    'use strict';
    /**
     * Do an AjaX GET request
     * @param url
     * @returns {Promise}
     */
    function xGet(url) {
        return new Promise(function (resolve, reject) {
            var req = new XMLHttpRequest();
            req.open('GET', url, true);
            req.onreadystatechange = function (e) {
                if (req.readyState == 4) {
                    if (req.status == 200) {
                        resolve(req.responseText, req);
                    }
                    else {
                        reject(req);
                    }

                }
            };
            req.send(null);
        });
    }

    return {
        get:xGet
    }
})();